#!/bin/bash
set -xpueo pipefail

TEMP_DIR="$(mktemp -d)"

function cleanup {
    rm -f "${TEMP_DIR}"/*.xml "${TEMP_DIR}"/*.dtd
    rmdir "${TEMP_DIR}"
}

trap cleanup EXIT

# Copy xml files to temp dir; as we're going to modify any https checkstyle dtd references - don't mess with workspace copies
cp *.xml "${TEMP_DIR}"

# Find all checkstyle https dtd urls; download them to temporary file and change references
# Note: Explicitly excluding ! from url regex; as it's used as sed delimiter
for dtd_url in $(grep -hEio '"https://[^!]+\.dtd"' *.xml  | tr -d '"' | sort | uniq); do
    DTD_FILE_NAME="$(basename "${dtd_url}")"
    TEMP_DTD_FILE_NAME="${TEMP_DIR}/${DTD_FILE_NAME}"
    wget -q "${dtd_url}" -O "${TEMP_DTD_FILE_NAME}"
    sed -i "s!${dtd_url}!${TEMP_DTD_FILE_NAME}!" "${TEMP_DIR}"/*.xml
done

dtd_validation() {
    xmllint --valid "${TEMP_DIR}/${1}" --noout
}

xml_validation() {
    xmllint "${TEMP_DIR}/${1}" --noout
}

## Import Control Files - DTD Validation
dtd_validation gateway-import-control.xml
dtd_validation test-gateway-import-control.xml
dtd_validation messagepay-import-control.xml
dtd_validation messagepay-test-import-control.xml
dtd_validation framework-checkstyle-import-control.xml
dtd_validation framework-test-checkstyle-import-control.xml

## suppression files - DTD validation
dtd_validation gateway-checkstyle-exclude.xml
dtd_validation test-gateway-checkstyle-exclude.xml
dtd_validation framework-checkstyle-exclude.xml
dtd_validation framework-test-checkstyle-exclude.xml

## checkstyle files - DTD validation
dtd_validation gateway-checkstyle.xml
dtd_validation messagepay-checkstyle.xml
dtd_validation messagepay-test-checkstyle.xml
dtd_validation test-gateway-checkstyle.xml
dtd_validation android-checkstyle.xml
dtd_validation framework-checkstyle.xml
dtd_validation framework-test-checkstyle.xml

## Other files - Basic xml validation
xml_validation framework-findbugs-exclude.xml
xml_validation framework-spotbugs-exclude.xml
xml_validation gateway-pmd.xml
xml_validation framework-pmd.xml
xml_validation framework-test-pmd.xml
xml_validation gateway-test-pmd.xml
xml_validation php-checkstyle.xml
# We can't do a proper validation, becausae the XSD does not exists anymore :(
xml_validation maven-update-versions-ruleset.xml

echo "Success!"
