/*
 * Copyright (c) Message4U Pty Ltd 2014-2015
 *
 * Except as otherwise permitted by the Copyright Act 1967 (Cth) (as amended from time to time) and/or any other
 * applicable copyright legislation, the material may not be reproduced in any format and in any way whatsoever
 * without the prior written consent of the copyright owner.
 */

package resources

String spockExclude = '*Spec,*Test,*IT'

ruleset {
  description 'Default messagemedia ruleset'

  AbstractClassName
  AbstractClassWithPublicConstructor
  AbstractClassWithoutAbstractMethod
  AddEmptyString
  AssertWithinFinallyBlock
  AssignCollectionSort
  AssignCollectionUnique
  AssignmentInConditional
  BigDecimalInstantiation
  BitwiseOperatorInConditional
  BooleanGetBoolean
  BooleanMethodReturnsNull
  BracesForClass
  BracesForForLoop
  BracesForIfElse
  BracesForMethod
  BracesForTryCatchFinally
  BrokenNullCheck
  BrokenOddnessCheck
  BuilderMethodWithSideEffects
  BusyWait
  CatchArrayIndexOutOfBoundsException
  CatchError
  CatchException
  CatchIllegalMonitorStateException
  CatchIndexOutOfBoundsException
  CatchNullPointerException
  CatchRuntimeException
  CatchThrowable
  ChainedTest
  ClassForName
  ClassName
  ClassNameSameAsFilename
  ClassSize
  //CloneWithoutCloneable
  CloneableWithoutClone
  CloseWithoutCloseable
  ClosureAsLastMethodParameter
  ClosureStatementOnOpeningLineOfMultipleLineClosure
  CollectAllIsDeprecated
  CompareToWithoutComparable
  ComparisonOfTwoConstants
  ComparisonWithSelf
  ConfusingClassNamedException
  ConfusingMethodName
  ConfusingMultipleReturns
  ConfusingTernary
  ConsecutiveBlankLines
  ConsecutiveLiteralAppends
  ConsecutiveStringConcatenation
  ConstantAssertExpression
  ConstantIfExpression
  ConstantTernaryExpression
  ConstantsOnlyInterface
  CouldBeElvis
  CoupledTestCase
  DeadCode
  DirectConnectionManagement
  DoubleCheckedLocking
  DoubleNegative
  DuplicateCaseStatement
  DuplicateImport
  //DuplicateListLiteral
  //DuplicateMapKey
  //DuplicateMapLiteral
  //DuplicateNumberLiteral
  DuplicateSetValue

  //DuplicateStringLiteral {
  //  doNotApplyToClassNames = spockExclude
  //}

  ElseBlockBraces
  EmptyCatchBlock
  EmptyClass
  EmptyElseBlock
  EmptyFinallyBlock
  EmptyForStatement
  EmptyIfStatement
  EmptyInstanceInitializer
  EmptyMethod
  EmptyMethodInAbstractClass
  EmptyStaticInitializer
  EmptySwitchStatement
  EmptySynchronizedStatement
  EmptyTryBlock
  EmptyWhileStatement
  EnumCustomSerializationIgnored
  EqualsAndHashCode
  EqualsOverloaded
  ExceptionExtendsError
  ExceptionNotThrown
  ExplicitArrayListInstantiation
  ExplicitCallToAndMethod
  ExplicitCallToCompareToMethod
  ExplicitCallToDivMethod
  ExplicitCallToEqualsMethod
  ExplicitCallToGetAtMethod
  ExplicitCallToLeftShiftMethod
  ExplicitCallToMinusMethod
  ExplicitCallToModMethod
  ExplicitCallToMultiplyMethod
  ExplicitCallToOrMethod
  ExplicitCallToPlusMethod
  ExplicitCallToPowerMethod
  ExplicitCallToRightShiftMethod
  ExplicitCallToXorMethod
  ExplicitGarbageCollection
  ExplicitHashSetInstantiation
  ExplicitLinkedHashMapInstantiation
  ExplicitLinkedListInstantiation
  ExplicitStackInstantiation
  ExplicitTreeSetInstantiation
  //FactoryMethodName
  FieldName
  FileCreateTempFile
  FileEndsWithoutNewline
  FinalClassWithProtectedMember
  ForLoopShouldBeWhileLoop
  ForStatementBraces
  GStringAsMapKey
  GStringExpressionWithinString
  GetterMethodCouldBeProperty
  GroovyLangImmutable
  HardCodedWindowsFileSeparator
  HardCodedWindowsRootDirectory
  HashtableIsObsolete
  IfStatementBraces
  IfStatementCouldBeTernary
  IllegalClassMember
  IllegalClassReference
  IllegalPackageReference
  IllegalRegex {
        regex = /\.sleep/
  }
  IllegalString
  IllegalSubclass
  ImplementationAsType
  ImportFromSamePackage
  ImportFromSunPackages
  InconsistentPropertyLocking
  InconsistentPropertySynchronization
  InsecureRandom
  IntegerGetInteger
  InterfaceName
  InvertedIfElse
  //JavaIoPackageAccess - This disallows java.io.File
  JdbcConnectionReference
  JdbcResultSetReference
  JdbcStatementReference

  LineLength {
     length = 200
  }

  LocaleSetDefault
  LoggerForDifferentClass
  LoggerWithWrongModifiers
  LoggingSwallowsStacktrace
  LongLiteralWithLowerCaseL
  MethodCount

  MethodName {
    doNotApplyToClassNames = spockExclude
  }

  MethodSize
  //MisorderedStaticImports
  MissingBlankLineAfterImports
  MissingBlankLineAfterPackage
  MissingNewInThrowStatement
  MultipleLoggers
  MultipleUnaryOperators
  NestedBlockDepth
  NestedSynchronization
  // Intellij create wildcard imports when organising imports
  //NoWildcardImports
  NonFinalPublicField
  NonFinalSubclassOfSensitiveInterface
  ObjectFinalize
  ObjectOverrideMisspelledMethodName
  PackageName
  ParameterName
  ParameterReassignment
  PrintStackTrace
  PrivateFieldCouldBeFinal
  PropertyName
  PublicFinalizeMethod
  PublicInstanceField
  RandomDoubleCoercedToZero
  RemoveAllOnSelf
  RequiredRegex
  RequiredString
  ReturnFromFinallyBlock
  ReturnNullFromCatchBlock
  ReturnsNullInsteadOfEmptyArray
  ReturnsNullInsteadOfEmptyCollection
  SerialPersistentFields
  SerialVersionUID
  SerializableClassMustDefineSerialVersionUID
  SimpleDateFormatMissingLocale
  SpaceAfterCatch
  SpaceAfterClosingBrace
  SpaceAfterComma
  SpaceAfterFor
  SpaceAfterIf
  SpaceAfterOpeningBrace
  SpaceAfterSemicolon
  SpaceAfterSwitch
  SpaceAfterWhile
  SpaceAroundClosureArrow

  // * Data-table based test with multiple map parameters (e.g. [From: '1235', To: '3234']) on separate lines align with colons breaking this rule.
  //SpaceAroundMapEntryColon {
  //  characterAfterColonRegex = /\s/
  //  characterBeforeColonRegex = /\S/
  //}

  SpaceAroundOperator
  SpaceBeforeClosingBrace
  SpaceBeforeOpeningBrace
  SpockIgnoreRestUsed
  StatelessClass
  StatelessSingleton
  StaticCalendarField
  StaticConnection
  StaticDateFormatField
  StaticMatcherField
  StaticSimpleDateFormatField
  SwallowThreadDeath
  SynchronizedMethod
  SynchronizedOnBoxedPrimitive
  SynchronizedOnGetClass
  SynchronizedOnReentrantLock
  SynchronizedOnString
  SynchronizedOnThis
  SynchronizedReadObjectMethod
  SystemErrPrint
  SystemExit
  SystemOutPrint
  SystemRunFinalizersOnExit
  TernaryCouldBeElvis
  ThisReferenceEscapesConstructor
  ThreadGroup
  ThreadLocalNotStaticFinal
  ThreadYield
  //The following checks are turned off because they are not useful for tests and we just use groovy for testing ;)
  //ThrowError
  //ThrowException
  //ThrowNullPointerException
  //ThrowRuntimeException
  //ThrowThrowable
  ThrowExceptionFromFinallyBlock
  ToStringReturnsNull
  TrailingWhitespace
  UnnecessaryBigDecimalInstantiation
  UnnecessaryBigIntegerInstantiation
  UnnecessaryBooleanExpression
  UnnecessaryBooleanInstantiation
  UnnecessaryCallForLastElement
  UnnecessaryCallToSubstring
  UnnecessaryCast
  UnnecessaryCatchBlock
  UnnecessaryCollectCall
  UnnecessaryCollectionCall
  UnnecessaryConstructor
  UnnecessaryDefInFieldDeclaration
  UnnecessaryDefInMethodDeclaration
  UnnecessaryDefInVariableDeclaration
  UnnecessaryDotClass
  UnnecessaryDoubleInstantiation
  UnnecessaryElseStatement
  UnnecessaryFail
  UnnecessaryFinalOnPrivateMethod
  UnnecessaryFloatInstantiation
//  UnnecessaryGString
//  UnnecessaryGetter
  UnnecessaryGroovyImport
  UnnecessaryIfStatement
  UnnecessaryInstanceOfCheck
  UnnecessaryInstantiationToGetClass
  UnnecessaryIntegerInstantiation
  UnnecessaryLongInstantiation
  UnnecessaryModOne
  UnnecessaryNullCheck
  UnnecessaryNullCheckBeforeInstanceOf
  // UnnecessaryObjectReferences
  UnnecessaryOverridingMethod
  UnnecessaryPackageReference
  UnnecessaryParenthesesForMethodCallWithClosure
  UnnecessaryPublicModifier
  UnnecessaryReturnKeyword
  UnnecessarySelfAssignment
  UnnecessarySemicolon
  UnnecessaryStringInstantiation
  UnnecessarySubstring
  UnnecessaryTernaryExpression
  UnnecessaryToString
  UnnecessaryTransientModifier
  UnsafeArrayDeclaration
  // UnsafeImplementationAsMap
  UnusedArray
  UnusedImport
  UnusedMethodParameter
  UnusedObject
  UnusedPrivateField
  UnusedPrivateMethod
  UnusedPrivateMethodParameter
  UnusedVariable
  UseAssertEqualsInsteadOfAssertTrue
  UseAssertFalseInsteadOfNegation
  UseAssertNullInsteadOfAssertEquals
  UseAssertSameInsteadOfAssertTrue
  UseAssertTrueInsteadOfAssertEquals
  UseAssertTrueInsteadOfNegation
  UseCollectMany
  UseCollectNested
  UseOfNotifyMethod
  VariableName
  VectorIsObsolete
  VolatileArrayField
  VolatileLongOrDoubleField
  WaitOutsideOfWhileLoop
  WhileStatementBraces

}
