FROM debian:stable-slim

RUN apt-get update && apt-get -y install wget sed libxml2-utils
RUN mkdir /app
ADD . /app
WORKDIR /app

CMD /app/validate.sh
