# standards

This project contains configurations used in other projects for:

* [Checkstyle](http://checkstyle.sourceforge.net) - plugin for checking code-convention compliance in java projects
* [Codenarc](http://codenarc.sourceforge.net/) - plugin for Groovy to ensure compliance with coding standards

## Reference

The ```java-development``` parental module refers to the checkstyle files in this project (```standards```) by URL into the repository.

## Testing Changes to the Rules

To test a configuration, edit this project locally and then copy the "checkstyle" mojo block from ```java-development``` into your local project's reactor pom and then run the build locally.  Note that this file then references other files such as ```gateway-import-control.xml``` and so their references may also need to be modified within the top-level ```gateway-checkstyle.xml``` file.

To get your local checkstyle mojo block, access your effective maven pom and find the ```maven-checkstyle-plugin``` plugin. 
Once copied into your local pom, adjust the ```configLocation``` to reference the corresponding local file paths in your Standards repo.
To test import controls you will also need to update the path of the property within the ```ImportControl``` module of framework-checkstyle.xml to your local path equivalent.

## Validating XML

Just run the ```validate.sh``` script. It will validate all the import control files in this project (xmllint cli tool required).

## IntelliJ Idea Settings

IntelliJ Idea has a few nice plugins that we can use to improve and unify our coding standards accross the teams which are using this beautiful IDE.

### Save Actions

[Save Actions](https://plugins.jetbrains.com/plugin/7642-save-actions) allows to format Java code so that the following coding standards are applied:

* Add final modifier to field.
* Add final modifier to local variable or parameter.
* Add this to field access.
* Add this to method access.
* Add missing @Override annotations.
* Add SerialVersionUID to serializable classes.
* Remove explicit generic type for diamond.
* Remove unnecessary @SuppressWarning annotation.
* Remove unnecessary semicolon.
* Optimize imports.

In order to get Save Actions settings to your Idea project just copy `idea/saveactions_settings.xml` to the `.idea` directory. 

### Importing Code Style

* Navigate to `File > Settings > Editor > Code Style`
* Click on the `Scheme` settings
* Choose `Import Scheme > IntelliJ IDEA code style XML`
* Select `code-style.xml` file in the modal dialog window and click `OK`
* Tick the `Current scheme` checkbox and click `OK`
